package com.example.bankingsystem.controller;

import com.blubank.doctorappointment.model.ConflictException;
import com.blubank.doctorappointment.model.NotAcceptableException;
import com.blubank.doctorappointment.model.ResourceNotFoundException;
import com.blubank.doctorappointment.model.appointment.Appointment;
import com.blubank.doctorappointment.model.appointment.AppointmentDto;
import com.blubank.doctorappointment.model.appointment.AppointmentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/appointments")
public class AppointmentCtrl {
    @Autowired
    private AppointmentRepository appointmentRepository;

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteAppointment(@PathVariable Long id, @RequestBody AppointmentDto appointmentDto) {
        Appointment appointment = appointmentRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("Appointment not found with id: " + id));

        if (appointment.getPatient() != null) {
            throw new NotAcceptableException("Cannot delete appointment that has been taken by a patient");
        }

        if (!appointment.getStartTime().equals(appointmentDto.getStartTime()) || !appointment.getEndTime().equals(appointmentDto.getEndTime())) {
            throw new ConflictException("Appointment has been modified since you last retrieved it");
        }
        appointmentRepository.delete(appointment);
        return ResponseEntity.noContent().build();
    }

    @GetMapping("/open")
    public ResponseEntity<List<AppointmentDto>> getOpenAppointments(@RequestParam(name = "doctorId") Long doctorId, @RequestParam(name = "date") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate date) {
        List<Appointment> appointments = appointmentRepository.findOpenAppointmentsByDoctorAndDate(doctorId, date);
        List<AppointmentDto> appointmentDtos = appointments.stream()
                .map(AppointmentDto::new)
                .collect(Collectors.toList());

        return ResponseEntity.ok(appointmentDtos);
    }
}
