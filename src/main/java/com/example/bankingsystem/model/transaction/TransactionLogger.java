package com.example.bankingsystem.model.transaction;

import org.springframework.stereotype.Component;

@Component
public class TransactionLogger implements TransactionObserver {

    @Override
    public void onTransaction(String accountNumber, String transactionType, double amount) {
        System.out.println("Transaction logged - Account: " + accountNumber + ", Type: " + transactionType + ", Amount: " + amount);
    }
}
