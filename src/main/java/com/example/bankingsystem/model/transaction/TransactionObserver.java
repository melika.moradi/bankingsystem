package com.example.bankingsystem.model.transaction;

public interface TransactionObserver {
    void onTransaction(String accountNumber, String transactionType, double amount);
}
