package com.example.bankingsystem.model.bankaccount;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class BankAccountService {

    @Autowired
    private BankAccountRepository bankAccountRepository;

    public BankAccount createAccount(String accountHolderName, double initialBalance) {
        BankAccount account = new BankAccount();
        account.setAccountHolderName(accountHolderName);
        account.setBalance(initialBalance);
        return bankAccountRepository.save(account);
    }

    public void deposit(Long accountId, double amount) {
        BankAccount account = bankAccountRepository.findById(accountId).orElseThrow(() -> new RuntimeException("Account not found"));
        account.setBalance(account.getBalance() + amount);
        bankAccountRepository.save(account);
    }

    public void withdraw(Long accountId, double amount) {
        BankAccount account = bankAccountRepository.findById(accountId).orElseThrow(() -> new RuntimeException("Account not found"));
        if (account.getBalance() >= amount) {
            account.setBalance(account.getBalance() - amount);
            bankAccountRepository.save(account);
        } else {
            throw new RuntimeException("Insufficient balance");
        }
    }

    public void transfer(Long fromAccountId, Long toAccountId, double amount) {
        withdraw(fromAccountId, amount);
        deposit(toAccountId, amount);
    }

    public double getBalance(Long accountId) {
        return bankAccountRepository.findById(accountId)
                .map(BankAccount::getBalance)
                .orElseThrow(() -> new RuntimeException("Account not found"));
    }
}
